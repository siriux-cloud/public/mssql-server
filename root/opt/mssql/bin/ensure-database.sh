#!/bin/bash

# Aseguramos que falle si algun comando falla
set -e
set -x

# Agregamos sqlcmd to PATH
PATH="$PATH:/opt/mssql-tools/bin"

# Servidor de base de datos
server="${MSSQL_SERVER}"
if [ -z "${server}" ]; then
    echo "No se ha especificado la variable de entorno MSSQL_SERVER ver https://learn.microsoft.com/en-us/sql/tools/sqlcmd/sqlcmd-utility?view=sql-server-ver16&tabs=odbc%2Clinux&pivots=cs1-bash#-s-protocolserverinstance_nameport"
    exit 1
fi

# Leer la variable de entorno SA_PASSWORD
sa_password="${MSSQL_SA_PASSWORD}"
if [ -z "${sa_password}" ]; then
    echo "No se ha especificado la variable de entorno MSSQL_SA_PASSWORD"
    exit 1
fi

# Database a crear
database="${MSSQL_DATABASE}"
if [ -z "${database}" ]; then
    echo "No se ha especificado la variable de entorno MSSQL_DATABASE"
    exit 1
fi

# Usuario a crear si no existe
username="${MSSQL_DATABASE_USER:-myuser}"
if [ -z "${username}" ]; then
    echo "No se ha especificado la variable de entorno MSSQL_DATABASE_USER"
    exit 1
fi

# Password del usuario a crear si no existe
password="${MSSQL_DATABASE_PASSWORD:-mypassword}"
if [ -z "${password}" ]; then
    echo "No se ha especificado la variable de entorno MSSQL_DATABASE_PASSWORD"
    exit 1
fi

echo "Ensure database ${database} on ${server}..."
query=$(cat <<EOF
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = '${database}')
BEGIN
    CREATE DATABASE [${database}];
END;
EOF
)
sqlcmd -S "${server}" -U sa -P "${sa_password}" -Q "${query}"

echo "Ensure login ${username} on ${database}..."
query=$(cat <<EOF
IF NOT EXISTS (SELECT name FROM sys.syslogins WHERE name = '${username}')
BEGIN
    CREATE LOGIN ${username} WITH PASSWORD = '${password}', DEFAULT_DATABASE = [${database}], CHECK_EXPIRATION = OFF, CHECK_POLICY = OFF;
END;
EOF
)
sqlcmd -S "${server}" -U sa -P "${sa_password}" -Q "${query}"

echo "Ensure user ${username} on ${database}..."
query=$(cat <<EOF
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'${username}')
BEGIN
    CREATE USER [${username}] FOR LOGIN [${username}] 
END;
EOF
)
sqlcmd -S "${server}" -d "${database}" -U sa -P "${sa_password}" -Q "${query}"

echo "Ensure role db_owner for ${username} on ${database}..."
query=$(cat <<EOF
IF NOT EXISTS (SELECT IS_ROLEMEMBER('db_owner', '${username}') WHERE IS_ROLEMEMBER('db_owner', '${username}') = 1)
BEGIN
    EXEC sp_addrolemember N'db_owner', N'${username}'
END;
EOF
)
sqlcmd -S "${server}" -d "${database}" -U sa -P "${sa_password}" -Q "${query}"

echo "Probamos el login"
sqlcmd -S "${server}" -d "${database}" -U "${username}" -P "${password}" -Q "SELECT 1;"

